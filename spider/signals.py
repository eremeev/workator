import django.dispatch


grab_task_done = django.dispatch.Signal(
    providing_args=[
        'title',
        'description',
        'task_url'])


class TaskStore(object):

    def send_task(self, title, description, task_url):
        grab_task_done.send(
            sender=self.__class__,
            title=title,
            description=description,
            task_url=task_url)


task_store = TaskStore()
