# -*- coding: utf-8 -*-

from grab.spider import Spider, Task
from grab import DataNotFound
import logging
from spider.signals import task_store


logging.basicConfig(level=logging.ERROR)


class BaseSpider(Spider):

    tasks_href_xpath = ''
    task_title_xpath = ''
    task_description_xpath = ''

    def task_initial(self, grab, task):
        for elem in grab.doc.select(self.tasks_href_xpath):
            url = grab.make_url_absolute(elem.attr('href'))
            yield Task('detail', url=url)

    def task_detail(self, grab, task):
        print task.url
        title = grab.doc.select(self.task_title_xpath).text()
        description = grab.doc.select(self.task_description_xpath).html()
        task_store.send_task(title, description, task.url)


class FreelanceRuSpider(BaseSpider):

    initial_urls = ['https://freelance.ru/']
    tasks_href_xpath = '//a[@class="ptitle"]'

    def task_detail(self, grab, task):
        print task.url

        try:
            title = grab.doc.select('//h3[@class="proj_title"]').text()
        except DataNotFound:
            title = grab.doc.select(
                '//h2[@class="alert alert-warning"]').text()

        try:
            description = grab.doc.select('//p[@class="txt href_me"]').html()
        except DataNotFound:
            description = grab.doc.select(
                '//div[@class="msg_notify alert alert-info"]').html()

        task_store.send_task(title, description, task.url)


class FreelansimRuSpider(BaseSpider):

    initial_urls = ['http://freelansim.ru/tasks']
    tasks_href_xpath = '//div[@class="task__title"]/a'
    task_title_xpath = '//h2[@class="task__title"]'
    task_description_xpath = '//div[@class="task__description"]'


class FreelancehuntComSpider(BaseSpider):

    initial_urls = ['http://freelancehunt.com/projects']
    tasks_href_xpath = '//a[contains(@href, "/project/show")]'
    task_title_xpath = '//div[@class="visible-xs"]'
    task_description_xpath = '//div[@class="well linkify-marker"]'

    def task_detail(self, grab, task):
        print task.url
        title = grab.doc.select(self.task_title_xpath).text()
        description = grab.doc.select(self.task_description_xpath).html()
        description = re.sub('data-cfemail="([a-z0-9]{42})"', '')
        task_store.send_task(title, description, task.url)


class DalanceRuSpider(BaseSpider):

    initial_urls = ['http://dalance.ru/']
    tasks_href_xpath = '//a[contains(@id, "project_title_")]'
    task_title_xpath = '//div[@class="project_title"]'
    task_description_xpath = '//div[@class="project_details_content"]'


class FreeLancersNetSpider(BaseSpider):

    initial_urls = ['http://www.free-lancers.net/']
    tasks_href_xpath = '//td[@width="70%"]/a[contains(@href, "/projects/")]'

    def task_detail(self, grab, task):
        title = grab.doc.select('//p[@class="headers"]').text_list()[1]
        description = grab.doc.select('//td[@class="main"]').html()
        task_store.send_task(title, description, task.url)


class ProhqRuSpider(BaseSpider):

    initial_urls = ['http://www.prohq.ru/projects/search/']
    tasks_href_xpath = '//section[@class="result"]/div/a'
    task_title_xpath = '//h1[@class="page-title"]'
    task_description_xpath = '//div[@class="task-text text-with-more"]'


class FreeLancingRuSpider(BaseSpider):

    initial_urls = ['http://www.free-lancing.ru/']
    tasks_href_xpath = '//div[@class="box"]/h4/a'
    task_title_xpath = '//*[@id="content"]/div[2]/div[2]/div[3]/div[1]/div[3]/div/h1'
    task_description_xpath = '//*[@id="content"]/div[2]/div[2]/div[3]/div[1]/div[3]/div/p'


class FreelanceJobRuSpider(BaseSpider):

    initial_urls = ['http://www.freelancejob.ru/']
    tasks_href_xpath = '//*[@id="lenta"]/div/div/a[1]'
    task_title_xpath = '//title'
    task_description_xpath = '/html/body/table/tr/td[1]/table[1]/tr/td[2]/table[3]/tr[1]/td/table/tr/td'


class WebpersonalRuSpider(BaseSpider):

    initial_urls = ['http://www.webpersonal.ru/']
    tasks_href_xpath = '//div[@class="data"]/h2/a'
    task_title_xpath = '//div[@class="data"]/h2/a'
    task_description_xpath = '/html/body/div[2]/div[4]/ul[1]/li/div[2]/p'


class BestLanceRuSpider(BaseSpider):

    initial_urls = ['http://www.best-lance.ru/']
    tasks_href_xpath = '//a[contains(@href, "/orders/order_")]'
    task_title_xpath = '//title'
    task_description_xpath = '//p[@class="p-desc"]'


class YammleComSpider(BaseSpider):

    initial_urls = ['http://www.yammle.com/projects']
    tasks_href_xpath = '//div[@class="oneprj"]/div/div/div/a'
    task_title_xpath = '//title'
    task_description_xpath = '//div[@class="text"]'


class WeblancerNetSpider(BaseSpider):

    initial_urls = ['http://www.weblancer.net/projects/']
    tasks_href_xpath = '//td[@class="il_main"]/a[@class="item"]'
    task_title_xpath = '//div[@class="title_box"]/h1'
    task_description_xpath = '//div[@class="id_description"]'
