from django.conf import settings
from django.utils.module_loading import import_by_path
from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):

    def handle(self, *args, **options):
        spider_modules = getattr(settings, 'SPIDERS', None)
        thread_number = getattr(settings, 'SPIDERS_THREAD_NUMBER', 1)
        if spider_modules:
            for spider_module in spider_modules:
                Spider = import_by_path(spider_module)
                spider = Spider(thread_number=thread_number)
                print 'run spider %s' % spider_module
                spider.run()
