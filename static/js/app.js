var TasksModel = function() {
	
	var self = this;
	self.tasks = ko.observable([]);
	self.cntSince = ko.observable();

	self.showEmptyText = ko.computed(function() {

		if( (self.tasks().length==0) && ( self.cntSince()==0 || self.cntSince()==null) ) {
			return true
		}
		else {
			return false
		}
	}, this);

	self.updateTasksSince = function(since, callback) {
		$.getJSON('/tasks_since', {'since': since}, function(data) {
			self.tasks(data.tasks);
			callback(data);
		});
	}

	self.updateTasks = function() {
		lastId = localStorage.getItem('lastId');
		self.updateTasksSince(lastId, function(data) {
			if(data.tasks.length>0) {
				lastId = data.tasks[0].id;
				localStorage.setItem('lastId', lastId);
				self.updateCnt()
			}
		});
	}

	self.updateCntSince = function(since) {
		$.getJSON('/tasks_cnt_since', {'since': since}, function(data){
			self.cntSince(data.count);
		});
	}

	self.updateCnt = function() {
		lastId = localStorage.getItem('lastId');
		self.updateCntSince(lastId);
	}

	self.showMore = function(data, event) {		
		$(event.target).parent().find('.description').slideToggle(100);
	}

}

model = new TasksModel();

$(document).ready(function() {

	ko.applyBindings(model);
	model.updateTasks();
	setInterval(function(){
			model.updateCnt()
		}, 15000);
});
