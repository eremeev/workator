FROM python:2.7
RUN mkdir /usr/src/workator
COPY req.txt /usr/src/workator/

WORKDIR /usr/src/workator/
RUN pip install -r req.txt
