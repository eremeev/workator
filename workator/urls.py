from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
                       # Examples:
                       # url(r'^$', 'work_aggregation.views.home', name='home'),
                       # url(r'^blog/', include('blog.urls')),

                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^tasks_history/',
                           'aggregator.views.tasks_history',
                           name='tasks_history'),
                       url(r'^tasks_since/', 'aggregator.views.tasks_since'),
                       url(r'^tasks_cnt_since/',
                           'aggregator.views.tasks_cnt_since'),
                       url(r'^$',
                           'aggregator.views.tasks_onlynew',
                           name='tasks_onlynew'),
                       )
