from django.shortcuts import render
from aggregator.models import Task
from django.core.paginator import Paginator
import json
from django.http import HttpResponse
from workator import settings

# Create your views here.


def tasks_history(request):
    page_num = request.GET.get('page', 1)
    tasks = Task.objects.all()
    paginator = Paginator(tasks, settings.PROJECTS_PER_PAGE)
    page = paginator.page(page_num)
    tasks = page.object_list
    return render(request,
                  'aggregator/tasks_history.html',
                  {'tasks': tasks,
                   'page': page})


def tasks_onlynew(request):
    return render(request, 'aggregator/tasks_onlynew.html', {})


def tasks_since(request):
    since = request.GET.get('since', None)

    if since:
        tasks = Task.objects.filter(id__gt=since)
    else:
        tasks = Task.objects.all()

    tasks = tasks[:settings.PROJECTS_PER_PAGE]

    data = []
    for task in tasks:
        data.append({
            'id': task.id,
            'title': task.title,
            'description': task.description,
            'task_url': task.task_url,
            'source': task.source(),
            'date_create': task.date_create.strftime('%D %T')
        })
    response = {'tasks': data}
    jsresponse = json.dumps(response)
    return HttpResponse(jsresponse, content_type="application/javascript")


def tasks_cnt_since(request):
    since = request.GET.get('since', None)
    if not since:
        since = 0
    count = Task.objects.filter(id__gt=since).count()
    jsdata = json.dumps({'count': count})
    return HttpResponse(jsdata, content_type='application/javascript')
