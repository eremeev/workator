from spider.signals import grab_task_done
from aggregator.models import Task


def save_result(sender, **kwargs):
    task_url = kwargs['task_url']
    if '#' in task_url:
        task_url = task_url[:task_url.find('#')]
    Task.objects.get_or_create(
        title=kwargs['title'],
        description=kwargs['description'],
        task_url=task_url
    )


def notify_result(sender, **kwargs):
    print '---------------------notify_result--------------------'
    print sender


grab_task_done.connect(save_result)
# grab_task_done.connect(notify_result)
