from django.contrib import admin
from aggregator.models import Task

# Register your models here.


class TaskAdmin(admin.ModelAdmin):
    list_display = ('title', 'date_create', 'task_url', 'source')
    readonly_fields = ('date_create', 'source')
    search_fields = ('title', 'description')

admin.site.register(Task, TaskAdmin)
