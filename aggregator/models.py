from django.db import models
from urlparse import urlparse

# Create your models here.


class Task(models.Model):
    title = models.CharField(max_length=128)
    description = models.TextField()
    task_url = models.URLField(max_length=512)
    date_create = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-id']

    def source(self):
        parse_result = urlparse(self.task_url)
        return parse_result.hostname

    def __unicode__(self):
        return self.title
